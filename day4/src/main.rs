use std::fs;

fn main() {
    let input = fs::read_to_string("/home/icar/Documents/rust/aoc2022/day4/input").unwrap();
    let result = input
        .trim()
        .lines()
        .filter(|pair| {
            let (elf1, elf2) = pair.split_at(pair.find(',').unwrap());
            let elf2 = &elf2[1..];

            let (elf1_start, elf1_end) = elf1.split_at(elf1.find('-').unwrap());
            let (elf2_start, elf2_end) = elf2.split_at(elf2.find('-').unwrap());

            let elf1_start = elf1_start.parse::<i32>().unwrap();
            let elf1_end = elf1_end[1..].parse::<i32>().unwrap();
            let elf2_start = elf2_start.parse::<i32>().unwrap();
            let elf2_end = elf2_end[1..].parse::<i32>().unwrap();

            ((elf1_start >= elf2_start && elf1_start <= elf2_end)
                || (elf1_end >= elf2_start && elf1_end <= elf2_end))
                || ((elf2_start >= elf1_start && elf2_start <= elf1_end)
                    || (elf2_end >= elf1_start && elf2_end <= elf1_end))
        })
        .count();

    println!("{result}");
}
