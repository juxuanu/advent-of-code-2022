use std::fs;

fn main() {
    // ...
    let input = fs::read_to_string("/home/icar/Documents/rust/aoc2022/day5/input").unwrap();
    let mut stacks = Vec::<Vec<char>>::with_capacity(9);
    init(&mut stacks);
    input.lines().skip(10).for_each(|line| {
        let mut split = line.split_whitespace();
        split.next();
        let moves = split.next().unwrap().parse::<usize>().unwrap();
        split.next();
        let from = split.next().unwrap().parse::<usize>().unwrap();
        split.next();
        let to = split.next().unwrap().parse::<usize>().unwrap();
        do_operation(&mut stacks, moves, from - 1, to - 1);
    });

    for stack in stacks {
        print!("{}", stack.last().unwrap());
    }
    println!("\n");
}

fn do_operation(stacks: &mut [Vec<char>], moves: usize, from: usize, to: usize) {
    let mut to_move = Vec::new();
    for _ in 0..moves {
        to_move.push(stacks[from].pop().unwrap());
    }
    to_move.reverse();
    stacks[to].extend_from_slice(&to_move);
}

fn init(stacks: &mut Vec<Vec<char>>) {
    // stacks.iter_mut().for_each(|s| *s = Vec::new());

    stacks.push(Vec::from(['b', 's', 'v', 'z', 'g', 'p', 'w']));
    stacks.push(Vec::from(['j', 'v', 'b', 'c', 'z', 'f']));
    stacks.push(Vec::from(['v', 'l', 'm', 'h', 'n', 'z', 'd', 'c']));
    stacks.push(Vec::from(['l', 'd', 'm', 'z', 'p', 'f', 'j', 'b']));
    stacks.push(Vec::from(['v', 'f', 'c', 'g', 'j', 'b', 'q', 'h']));
    stacks.push(Vec::from(['g', 'f', 'q', 't', 's', 'l', 'b']));
    stacks.push(Vec::from(['l', 'g', 'c', 'z', 'v']));
    stacks.push(Vec::from(['n', 'l', 'g']));
    stacks.push(Vec::from(['j', 'f', 'h', 'c']));
}
