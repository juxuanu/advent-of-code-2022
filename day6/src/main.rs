use std::collections::{HashSet, VecDeque};
use std::fs;
use std::hash::Hash;

fn main() {
    let input: String = fs::read_to_string("/home/icar/Documents/rust/aoc2022/day6/input")
        .unwrap()
        .lines()
        .take(1)
        .collect();

    let mut count = 14;
    let mut buffer = input.chars().take(14).collect::<VecDeque<char>>();

    for c in input.chars().skip(14) {
        if has_unique_elements(buffer.iter()) {
            break;
        }
        buffer.pop_front();
        buffer.push_back(c);
        count += 1;
    }

    println!("{count}");
}

//     [a b c d] e f | count = 4
//   a [b c d e] f   | count = 5
// a b [c d e f]     | count = 6

fn has_unique_elements<T>(iter: T) -> bool
where
    T: IntoIterator,
    T::Item: Eq + Hash,
{
    let mut uniq = HashSet::new();
    iter.into_iter().all(move |x| uniq.insert(x))
}
