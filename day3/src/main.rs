#![feature(iter_array_chunks)]

use std::fs;

fn main() {
    let input = fs::read_to_string("/home/icar/Documents/rust/aoc2022/day3/input").unwrap();

    let total = input
        .lines()
        .into_iter()
        .array_chunks::<3>()
        // calcular valor per cada rucksack i sumar
        .map(|group| {
            group[0]
                .chars()
                .filter(|c| group[1].contains(*c) && group[2].contains(*c))
                .take(1)
                .map(|repeated_type| {
                    match repeated_type.is_uppercase() {
                        // ascii: A = 65, a = 97
                        true => (u32::from(repeated_type) - 38) as i32,
                        false => (u32::from(repeated_type) - 96) as i32,
                    }
                })
                .sum::<i32>()
        })
        .sum::<i32>();

    println!("{total}");
}
