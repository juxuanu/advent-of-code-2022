use std::fs;

fn main() {
    let file_contents = fs::read_to_string("/home/icar/Documents/rust/aoc2022/day1/input")
        .expect("Unable to read the file");
    let elfs: Vec<&str> = file_contents.split("\n\n").collect();
    let mut total_calories_elf: Vec<usize> = Vec::new();
    elfs.iter()
        .for_each(|&e| total_calories_elf.push(elf_calories(e)));
    total_calories_elf.sort_unstable();
    let max_3_sum = total_calories_elf[total_calories_elf.len() - 3..]
        .iter()
        .sum::<usize>();
    println!("max: {}", max_3_sum);
}

fn elf_calories(calories_list: &str) -> usize {
    let mut sum: usize = 0;
    calories_list
        .split('\n')
        .for_each(|snack| sum += snack.parse().unwrap_or(0));
    sum
}
