use std::fs;

fn calculate_score(guide: &str) -> u32 {
    let mut total_score = 0;

    // Split the guide into individual rounds and iterate over each round.
    for round in guide.trim().split('\n') {
        // Split the round into the two moves (your opponent's move and your move).
        let moves: Vec<char> = round
            .trim()
            .split(' ')
            .take(2)
            .map(|s| s.chars().next().unwrap())
            .collect();
        let opponent_move = moves[0];
        let your_move = moves[1];

        // Calculate the score for the round.
        let mut round_score = 0;
        match opponent_move {
            // Opponent played Rock.
            'A' => {
                if your_move == 'Y' {
                    round_score = 1 + 3;
                } else if your_move == 'Z' {
                    round_score = 2 + 6;
                } else {
                    round_score = 3 + 0;
                }
            }
            // Opponent played Paper.
            'B' => {
                if your_move == 'X' {
                    round_score = 1 + 0;
                } else if your_move == 'Z' {
                    round_score = 3 + 6;
                } else {
                    round_score = 2 + 3;
                }
            }
            // Opponent played Scissors.
            'C' => {
                if your_move == 'X' {
                    round_score = 2 + 0;
                } else if your_move == 'Y' {
                    round_score = 3 + 3;
                } else {
                    round_score = 1 + 6;
                }
            }
            _ => (),
        }

        // Add the score for the round to the total score.
        total_score += round_score;
    }

    total_score
}

fn main() {
    let guide = fs::read_to_string("/home/icar/Documents/rust/aoc2022/day2/input").unwrap();
    let total_score = calculate_score(&guide);
    println!("Total score: {}", total_score);
}
